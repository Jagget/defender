﻿using UnityEngine;
using System.Collections;

public class Position : MonoBehaviour
{

	void OnDrawGizmos ()
	{
		Gizmos.DrawWireSphere (GetComponent<Transform> ().position, 0.5f);
	}
}
