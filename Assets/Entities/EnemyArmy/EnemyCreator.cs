﻿using UnityEngine;

public class EnemyCreator : MonoBehaviour
{
	public GameObject EnemyPrefab;
	public float Width = 10.0f;
	public float Hight = 5.0f;
	public float Speed = 2f;
	Transform Transf;
	bool MovingRight = true;
	float xmax;
	float xmin;

	// Use this for initialization
	void Start ()
	{
		Transf = GetComponent<Transform> ();
		
		float zDistance = Transf.position.z - Camera.main.transform.position.z;
		
		Vector3 leftBottom = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, zDistance));
		Vector3 rightBottom = Camera.main.ViewportToWorldPoint (new Vector3 (1, 0, zDistance));
		
		xmin = leftBottom.x;
		xmax = rightBottom.x;
		
		foreach (Transform child in Transf) {
			GameObject enemy = Instantiate (EnemyPrefab, child.transform.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = child;
		}
	}
	
	void OnDrawGizmos ()
	{
		Gizmos.DrawWireCube (GetComponent<Transform> ().position, new Vector3 (Width, Hight));
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (MovingRight) {
			Transf.position += Vector3.right * Speed * Time.deltaTime;
		} else {
			Transf.position += Vector3.left * Speed * Time.deltaTime;
		}
		
		float armyLeftEnge = Transf.position.x - Width * 0.5f;
		float armyRightEnge = Transf.position.x + Width * 0.5f;
		
		if (armyLeftEnge <= xmin) {
			MovingRight = true;
		} else if (armyRightEnge >= xmax) {
			MovingRight = false;
		}
	}
}
