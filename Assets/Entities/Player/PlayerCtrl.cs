﻿using UnityEngine;

public class PlayerCtrl : MonoBehaviour
{
	public float Speed = 7f;
	public float LaserSpeed = 5f;
	public float FiringRate = 0.333f;
	public GameObject LaserPrefab;
	Transform Transf;
	float xmax;
	float xmin;
	float Padding = 0.55f;

	// Use this for initialization
	void Start ()
	{
		Transf = GetComponent<Transform> ();
		
		float zDistance = Transf.position.z - Camera.main.transform.position.z;
		
		Vector3 leftBottom = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, zDistance));
		Vector3 rightBottom = Camera.main.ViewportToWorldPoint (new Vector3 (1, 0, zDistance));
		
		xmin = leftBottom.x + Padding;
		xmax = rightBottom.x - Padding;
	}
	
	void Fire ()
	{
		GameObject laserShot = Instantiate (LaserPrefab, Transf.position, Quaternion.identity) as GameObject;
		laserShot.rigidbody2D.velocity = new Vector2 (0f, LaserSpeed);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKey (KeyCode.LeftArrow)) {
			Transf.position += Vector3.left * Speed * Time.deltaTime;
		} else if (Input.GetKey (KeyCode.RightArrow)) {
			Transf.position += Vector3.right * Speed * Time.deltaTime;
		}
		
		if (Input.GetKeyDown (KeyCode.Space)) {
			InvokeRepeating ("Fire", 0.00001f, FiringRate);
		}
		
		if (Input.GetKeyUp (KeyCode.Space)) {
			CancelInvoke ("Fire");
		}
		
		float newx = Mathf.Clamp (Transf.position.x, xmin, xmax);
		Transf.position = new Vector3 (newx, Transf.position.y, Transf.position.z);
	}
}
