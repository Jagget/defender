﻿using UnityEngine;

public class LevelManger : MonoBehaviour
{
	public void LoadLevel (string levelName)
	{
		Debug.Log ("Level load requested for: " + levelName);
		Application.LoadLevel (levelName);
	}

	public void LoadNextLevel ()
	{
		Application.LoadLevel (Application.loadedLevel + 1);
	}
	
	public void QuitRequest ()
	{
		Debug.Log ("Qiut requested");
		Application.Quit ();
	}
	
	public void BrickDestroyed ()
	{
	}
}
